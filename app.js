'use strict';
const
    express         = require('express');

let
    app             = express(),
    port            = process.env.PORT || 3000;

app.set('view engine', 'ejs');

app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
    res.render('index.ejs');
});

app.listen(port, function () {
    console.log(`[ APPLICATION ] Server is now listening to port ${port}`);
});